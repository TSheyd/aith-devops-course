"""
Simple ETL (Extract-Transform-Load) Pipeline as an example of a DAG
This pipeline Extracts some stock data, transforms it and prints it / loads in a database
"""

from pyspark.sql import SparkSession
from pyspark import SparkConf

import yfinance
import pandas as pd

conf = SparkConf().setAppName('sparkExapmle').setMaster("spark://spark-master:7077")


def extract() -> pd.DataFrame:
    # Some random tickers. IRL this would be a read from a database
    tickers = ['AAPL', 'MSFT', 'WMT']

    df = pd.DataFrame()

    for ticker in tickers:
        stock = yfinance.Ticker(ticker)  # Yahoo Finance API
        data = stock.income_stmt.T
        data['ticker'] = ticker
        df = pd.concat([df, data])
    return df


def transform(df: pd.DataFrame):
    df.index = pd.to_datetime(df.index).date
    df = df.reset_index(drop=False, names='reportedDate')
    df = df.groupby('ticker').first()  # keep last reports only
    return df


def load(df: pd.DataFrame):
    # Yes, Spark is very useful here, but it's very stubborn when it comes to writing locally...
    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    sdf = spark.createDataFrame(df)
    sdf.toPandas().to_excel("ticker_reports_spark.xlsx")
    return 0


# Execution
if __name__ == '__main__':
    data = extract()
    summary = transform(data)
    load(summary)
