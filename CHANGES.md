# Changelog

### 2023-10-31 - Lab 4 commit
#### Added
- Added logging&monitoring pipeline with Zabbix, Loki and Grafana.

### 2023-10-25 - Lab 3 commit
#### Added
- Enabled CI/CD by packaging the project in a gitlab-runner

### 2023-10-18 - Lab 2 commit
#### Added
- Started CHANGES.md
- Added support of scheduling Spark jobs. See "_fetch_ticker_prices_Spark_" for a working example.