FROM apache/airflow:2.7.1

# Set the working directory and airflow path
ENV AIRFLOW_HOME=/opt/airflow
WORKDIR $AIRFLOW_HOME

# Update packages, install Spark dependencies
USER root
RUN apt update && apt -y install procps default-jre

# Create default subdirectories, add packages from requirements.txt
USER airflow

# Copy necessary files/dirs and adjust priveleges
COPY --chown=airflow . .

ADD requirements.txt .
RUN pip install apache-airflow==${AIRFLOW_VERSION} -r requirements.txt && rm requirements.txt && mkdir -p data/
