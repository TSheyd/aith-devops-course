from airflow import DAG
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator

from datetime import datetime, timedelta


default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=2)
}

dag = DAG(
    dag_id='fetch_stock_reports_Spark',
    default_args=default_args,
    description='An example DAG for Spark',
    schedule_interval='@daily',
    start_date=datetime(2023, 10, 10),
    catchup=False
)

spark_job = SparkSubmitOperator(
    task_id='sparkExample',
    application='spark/spark_task.py',
    name='sparkExample',
    conn_id='spark_example',
    dag=dag
)

spark_job