from airflow.decorators import dag, task

import yfinance
import pandas as pd
from datetime import datetime


# Decorator seemed like a cleaner choice for me than what was listed in examples
@dag(
    dag_id='fetch_stock_reports',
    schedule_interval='@weekly',
    start_date=datetime(2023, 9, 25),
    tags=['example']
)
def example_etl():
    """
    Simple ETL (Extract-Transform-Load) Pipeline as an example of a DAG
    This pipeline Extracts some stock data, transforms it and prints it / loads in a database
    """
    @task()
    def extract() -> pd.DataFrame:
        """
        Extract task
        """

        # Some random tickers. IRL this would be a read from a database
        tickers = ['AAPL', 'MSFT', 'WMT']

        df = pd.DataFrame()

        for ticker in tickers:
            stock = yfinance.Ticker(ticker)  # Yahoo Finance API
            data = stock.income_stmt.T
            data['ticker'] = ticker

            df = pd.concat([df, data])

        return df

    @task()
    def transform(df: pd.DataFrame):
        """
        Transform task
        """
        df.index = pd.to_datetime(df.index).date
        df = df.reset_index(drop=False, names='reportedDate')
        df = df.groupby('ticker').first()  # keep last reports only
        return df

    @task()
    def load(df: pd.DataFrame):
        """
        Load task
        This would load data into a database, but here I just save it in Excel (/opt/airflow/data/ticker_reports.xlsx).
        "data" directory is created in Dockerfile
        """
        df.to_excel("data/ticker_reports.xlsx", index=True, header=True)
        return 0

    # Execution
    data = extract()
    summary = transform(data)
    load(summary)


etl_dag = example_etl()