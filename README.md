# AITH-DevOps-Course

AI Talent Hub DevOps Course, 1st semester.

This project runs Apache Airflow in a Docker Container set up with a custom Dockerfile.


## Deploying and running
### CI/CD
2. Clone repo: `git clone ...`
2. Run `docker-compose --project-directory ./ -f runner-compose.yml up -d` to launch gitlab-runner
3. Go to repo on [gitlab.com](https://gitlab.com/) **Settings → CI/CD → Runners** and copy runner registration token 
(under 3 dots near *New Project Runner* button)
4. Back in cmd, run `docker exec -it gitlab-runner gitlab-runner register --url https://gitlab.com/ --clone-url https://gitlab.com/`
5. Proceed with entering copied token, writing description and specifying runner tag **docker_9281** (same as in _.gitlab.ci.yml).
6. After creating a runner, enter the container with `docker exec -it gitlab-runner bash` and add `volumes = ["/var/run/docker.sock:/var/run/docker.sock", "/cache"]` 
in the end of `etc/gitlab-runner/config.toml` file
7. Next, go to the gitlab page and navigate to **Build → Pipelines**. If everything works well, there will be a pipeline already running. 
8. Wait untill the **deploy** step completes and proceed to using the application.

### Using
1. Open `http://localhost:8080/` and enter default credentials (**airflow: airflow**)
2. To connect Spark, go to **Admin → Connections → New** in Airflow top panel and create a new connection:
    - set Connection id = **spark_example** (should match `task_id` in example_spark_dag.py),
    - set Connection type = **Spark**,
    - set host = ** spark://spark-master**,
    - set port = **7077**,
    - Click "Save" and go back to the main page.
3. Trigger example DAG
4. View run results in data directory (`/opt/airflow/data/`)


## DAG contents

Airflow Contains two example DAGs with an ETL pipeline of 3 tasks:
1. **E**xtract financial reports data for some companies from Yahoo Finance with [yfinance](https://github.com/ranaroussi/yfinance)
2. **T**ransform the data (keep only latest reports, place "ticker" and "date" columns first)
3. **L**oad the data (here I used loading in an Excel file)

Both DAGs serve the same purpose, but `fetch_stock_prices` uses pure Airflow and `fetch_stock_prices_Spark`
uses Spark for executing a job.

Both Extract and Load steps usually interact with a database, but here I simplified the workflow as it's just an example.

---
### References:
1. [Airflow Docs](https://airflow.apache.org/docs/apache-airflow/stable/index.html)
2. [Docker Docs](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
3. This [Medium article](https://medium.com/swlh/using-airflow-to-schedule-spark-jobs-811becf3a960) on scheduling Spark jobs with Airflow
